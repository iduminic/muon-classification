//To run the TMVAClassification.C file, we are using the command:

root -l ./TMVAClassification.C\(\"Fisher,Likelihood\"\)

//The command actually runs in root and is mandatory to specify the method of the TMVA classification. In this case, we make use of the Fisher and Likelihood methods.

//Check how much memory you are using on lxplus. You start with 2Gb and you can update it up to 10Gb. Follow the short tutorial on: https://resources.web.cern.ch/resources/Help/?kbid=067040

//In the ROOT_Analysis folder, there is another TMVA classification file: TMVAClassification2.C. This one makes use of the BDTG method in order to distinguish between signal (the cosmic muons) and background (which in this case is considered to be the collision data).

//The difference between the 2 C++ files is that one of them uses the Fisher method to train the decision trees, while the other one is base on the BDTG method.

root -l ./TMVAClassification2.C\(\"BDTG\"\)

//Both commands worked fine. The second one produces a GUI that is immediately opened in ROOT and it makes all plots easily accesible. These 2 .C files were run on the lxplus server.
